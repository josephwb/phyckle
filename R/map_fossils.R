
##' @title Map Fossil Observation To A Time-calibrated Tree
##' @description Record where in a time-calibrated tree fossil observations
##' occurred. 
##' @param phy a full (i.e., not pruned of extinct lineages) time-calibrated
##' phylogeny produced by \code{sim_fossils} and of class \code{phyckle}. The 
##' tree has two extra attributes: \code{$fossils}, a list of fossil observation
##' times by edge, and \code{extinct}, a Boolean vector indicating whether an 
##' edge is extinct. Both of these use the same edge ordering as \code{phy$edge}
##' @return A list with the following items:
##' \itemize{
##' \item {\code{$mapped.fossils}} A dataframe containing information for all
##' simulated fossils, organized into the following columns:
##' \itemize{
##' \item {\code{fossilID}} the fossil ID
##' \item {\code{fossilAge}} the age of the fossil, measured from the present
##' \item {\code{extantNode}} the node ID of the closest rootward node which
##' has left extant descendants. Note this node ID corresponds to the
##' \code{$extant.tree} above, not \code{$bd.tree}
##' \item {\code{nodeAge}} the age of the \code{extantNode} above
##' \item {\code{bd.anc}} the node ID of the direct ancestral node to the
##' fossil in the complete \code{$bd.tree}
##' \item {\code{bd.dec}} the node ID of the direct descendant node to the
##' fossil in the complete \code{$bd.tree}
##' \item {\code{lTaxon}} in \code{$extant.tree}, an extant descendant tip on
##' the (arbitrary) left side of \code{extantNode}. Used by some divergence time
##' estimation methods to identify MRCA nodes
##' \item {\code{rTaxon}} in \code{$extant.tree}, an extant descendant tip on
##' the (arbitrary) right side of \code{extantNode}. Used by some divergence
##' time estimation methods to identify MRCA nodes
##' }
##' \item {\code{$oldest.fossils}} A subsetted dataframe of the one above,
##' containing only the oldest fossils which could be used to calibrate a node
##' which has left extant descendants
##' \item {\code{$par}} A list containing the simulation parameters: \code{b},
##' \code{d}, \code{r}, and \code{seed}
##' }
##' @seealso \code{\link{sim_fossils}} the function which  generates the fossil
##' samples
##' @export
map_fossils <- function (phy) {
    extinctTaxa <- geiger::is.extinct(phy);
    extant <- get_extant_nodes(phy);
    nodeHeights <- geiger::heights(phy);
    
    extantTree <- ape::drop.tip(phy=phy, extinctTaxa);
    
    fossilCounts <- unlist(lapply(phy$fossils, function (x) length(stats::na.exclude(x))));
    
    constrainedNodes <- NULL;
    constrainedAges <- NULL;
    trueAges <- NULL;
    bd.anc <- NULL;
    bd.dec <- NULL;
    
    for (i in 1:length(phy$fossil)) {
        fAges <- phy$fossils[[i]];
        if (fossilCounts[i] > 0) {
            node <- phy$edge[i,1];
            while (!node %in% extant) { # walk back until an extant node is encountered
                node <- phy$edge[which(phy$edge[,2] == node),1];
                if (length(node) == 0) {
                    constrainedNodes <- c(constrainedNodes, rep(NA, length(fAges)));
                    constrainedAges <- c(constrainedAges, fAges);
                    trueAges <- c(trueAges, rep(NA, length(fAges)));
                    bd.anc <- c(bd.anc, rep(phy$edge[i,1], length(fAges)));
                    bd.dec <- c(bd.dec, rep(phy$edge[i,2], length(fAges)));
                    break;
                }
            }
            if (length(node) != 0) {
                nodeAge <- nodeHeights[node, 2]; # true age of the node to be constrained
                constrainedNodes <- c(constrainedNodes, rep(node, length(fAges)));
                constrainedAges <- c(constrainedAges, fAges);
                trueAges <- c(trueAges, rep(nodeAge, length(fAges)));
                bd.anc <- c(bd.anc, rep(phy$edge[i,1], length(fAges)));
                bd.dec <- c(bd.dec, rep(phy$edge[i,2], length(fAges)));
            }
        }
    }
    leftChild <- character(length=length(constrainedNodes));
    rightChild <- character(length=length(constrainedNodes));
    trueAge <- numeric(length=length(constrainedNodes));
    extantNode <- integer(length=length(constrainedNodes));
    
    for (i in 1:length(constrainedNodes)) {
        if (!is.na(constrainedNodes[i])) {
            children <- phy$edge[which(phy$edge[,1] == constrainedNodes[i]),2];
            lTips <- geiger::tips(phy, children[1]);
            leftChild[i] <- lTips[!lTips %in% extinctTaxa][1];
            rTips <- geiger::tips(phy, children[2]);
            rightChild[i] <- rTips[!rTips %in% extinctTaxa][1];
            extantNode[i] <- ape::getMRCA(extantTree, c(leftChild[i], rightChild[i]));
        } else {
            leftChild[i] <- rightChild[i] <- extantNode[i] <- NA;
        }
    }
    fossilID <- paste("fossil", 1:length(constrainedNodes), sep = "");
    mapped.fossils <- data.frame(fossilID=fossilID, fossilAge=constrainedAges,
                                 nodeAge=trueAges, extantNode=extantNode,
                                 lTaxon=leftChild, rTaxon=rightChild,
                                 bd.anc=bd.anc, bd.dec=bd.dec);
    return(mapped.fossils);
}


# get nodes that, after pruning extinct lineages, will appear in the pruned tree
# the first node returned will be the crown group MRCA
get_extant_nodes <- function (phy) {
    extant <- numeric();
    nodes <- unique(phy$edge[!phy$extinct,1]);
    for (i in nodes) {
        if (all(!phy$extinct[which(phy$edge[,1] == i)])) {
            extant <- c(extant, i);
        }
    }
    return(sort(extant));
}

# from a precalculated table of fossil ages, grab just the oldest ones
# also checks if nested fossils are older; if so, drops the deeper fossil
# the phy passed in the the extant-only tree
get_oldest_fossils <- function (phy, mapped.fossils) {
    # reorder so that calibrated nodes are together, with oldest first *** <- do this earlier?
    fossils <- mapped.fossils[with(mapped.fossils, order(lTaxon, rTaxon, -fossilAge)), ];
    nodes <- unique(fossils[,c("lTaxon", "rTaxon")]); # filter by left- and right-children
    
    # grab oldest fossil for each calibrated node
    idx <- match(paste(nodes$lTaxon, "_", nodes$rTaxon, sep=""), paste(fossils$lTaxon, "_", fossils$rTaxon, sep=""));
    fossils <- fossils[idx,];
    
    # drop any fossils outside of ingroup *** <- do we want to do this?
    stem.fossils <- which(is.na(fossils$nodeAge));
    if (length(stem.fossils) > 0) {
        fossils <- droplevels(fossils[-stem.fossils,]);
    }
    
    # now, check if nested fossils are older. if so, drop the fossil
    constrainedNodes <- as.numeric(apply(fossils[,c("lTaxon", "rTaxon")], 1, function (x) ape::getMRCA(phy, x)));
    
    # index of fossils to drop
    toDrop <- numeric();
    
    for (i in 1:length(constrainedNodes)) {
        desc <- phangorn::Descendants(phy, constrainedNodes[i], type="all");
        idx <- which(constrainedNodes %in% desc);
        if (length(idx) > 0) {
            if (which.max(fossils[c(i, idx), "fossilAge"]) != 1) {
                toDrop <- c(toDrop, i);
            }
        }
    }
    if (length(toDrop) > 0) {
        fossils <- droplevels(fossils[-toDrop,]);
    }
    return(fossils);
}

# fossils is a dataframe containing all mapped.fossils (not just oldest)
extract_strat_ranges <- function (fossils) {
    nodes <- unique(stats::na.omit(fossils$extantNode));
    nodeAge <- fossils$nodeAge[match(nodes, fossils$extantNode)];
    n <- length(nodes);
    minAge <- maxAge <- range <- numeric(n);
    sampSize <- integer(n);
    for (i in 1:n) {
        idx <- which(fossils$extantNode == nodes[i]);
        ages <- fossils$fossilAge[idx];
        minAge[i] <- min(ages);
        maxAge[i] <- max(ages);
        range[i] <- max(ages) - min(ages);
        sampSize[i] <- length(ages);
    }
    strat <- data.frame(extantNode=nodes, nodeAge=nodeAge, minAge=minAge,
                        maxAge=maxAge, range=range, sampSize=sampSize);
    return(strat);
}
