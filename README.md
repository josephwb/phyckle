[![Build Status](https://travis-ci.com/josephwb/phyckle.svg?branch=master)](https://travis-ci.com/bitbucket/josephwb/phyckle)

Phyckle: A Tree, Fossil, and Character Simulator
---------------
![](phyckle_sim.png)

Overview
--------------
A tool for addressing issues of model adequacy in phylogenetic divergence time
estimation, phyckle provides the ability to simulate time-calibrated
phylogenetic trees according to some diversification model, fossil observations
according to a constant rate Poisson process, and characters data (DNA or
discrete morphological data) for both extant and extinct lineages under a wide
range of rate models.
**Note:** Character simulation code exists, but has not yet been added to this
package. Coming soon!

Installation
---------------
To install, simply do the following:
```
install.packages("devtools");
devtools::install_bitbucket("josephwb/phyckle");
```

Usage
---------------
The main function, which wraps most others, is `sim_data`. The main arguments are:

1. b: the per-lineage speciation (birth) rate
2. d: the per-lineage extinction (death) rate
3. r: the (homogeneous) instantaneous fossil sampling rate
4. ntax: the number of extant lineages to simulate



Do:

```
?sim_data
```
for further information on the inputs/outputs.
